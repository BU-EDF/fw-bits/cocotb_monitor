import random
import logging
import cocotb
import math

#monitoring stuff
import time
from datetime import datetime

#cocotb things
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, ReadOnly, ClockCycles, Join, Combine
#from cocotb.drivers import BitDriver
from cocotb.regression import TestFactory
#from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure
#for get_sim_time
import cocotb.utils as utils
from decimal import Decimal

def default_print_function():
    return ""
class sim_monitor(object):

    def __init__(self,update_period=10,print_function=default_print_function):        
        self.monitoring = print_function

        self.log = logging.getLogger('cocotb.testbench')

        self.UPDATE_WAIT_TIME_S = update_period #s
        self.DEFAULT_SIM_RATE   = 10.0 #ns/s
        self.sim_rate = self.DEFAULT_SIM_RATE #ns/s
        self.current_wall_time = time.time()
        self.current_sim_time  = utils.get_sim_time(units='ns')
                
    async def RunMonitoringWait(self):
        #store previous times
        last_sim_time  = self.current_sim_time
        last_wall_time = self.current_wall_time

        self.current_wall_time = time.time()
        self.current_sim_time  = utils.get_sim_time(units='ns')

        self.sim_rate = ( float(self.current_sim_time  - last_sim_time) /
                          float(self.current_wall_time - last_wall_time))

        #check for somewhat sensable values. 
        if self.sim_rate <= self.DEFAULT_SIM_RATE or math.isinf(self.sim_rate):
            self.sim_rate = self.DEFAULT_SIM_RATE

        #start computing the sim_rate from Timers
        await Timer(math.floor(self.sim_rate*self.UPDATE_WAIT_TIME_S),units='ns')
        
    async def SimMonitor(self):        
        while True:
            await self.RunMonitoringWait()
            current_sim_time = utils.get_sim_time(units='ns')
            self.log.info("%s (Sim T:% 9d ns, % 7.0f ns/s): %- 35s" % 
                  (datetime.now().strftime("%H:%M:%S"),
                   current_sim_time,
                   self.sim_rate,
                   self.monitoring()))
